﻿using Microsoft.AspNetCore.Mvc;
using System.Runtime.InteropServices.JavaScript;
using System.Text.Json.Nodes;
using PeopleApi.Models;
using System.Text.Json;
using System.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PeopleApi.Controllers
{
    [Route("api/people")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        // GET: api/people
        [HttpGet]
        public async Task<IActionResult> GetPeople()
        {
            HttpClient client = new HttpClient();
            string url = string.Format("http://localhost:5236/api/user");
            HttpResponseMessage response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                dynamic json = JsonObject.Parse(responseString);
                return new JsonResult(json);
            }
            throw new Exception("Internal Server Error");
        }

        // GET api/people/role
        [HttpGet("/role")]
        public async Task<IActionResult> GetRole()
        {
            HttpClient client = new HttpClient();
            string url = string.Format("http://localhost:5038/api/role");
            HttpResponseMessage response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                dynamic json = JsonObject.Parse(responseString);
                return new JsonResult(json);
            }
            throw new Exception("Internal Server Error");
        }

        // POST api/people
        [HttpPost]
        public async Task<IActionResult> AddPeople([FromBody] PeopleModel body)
        {
            HttpClient client = new HttpClient();
            var people = JsonSerializer.Serialize(body);
            var requestContent = new StringContent(people, Encoding.UTF8, "application/json");
            string url = string.Format("http://localhost:5236/api/user");
            HttpResponseMessage response = client.PostAsync(url, requestContent).Result;
            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                dynamic json = JsonObject.Parse(responseString);
                return new JsonResult(json);
            }
            throw new Exception("Internal Server Error");
        }

        // PUT api/people/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPeople(int id, [FromBody] UpdatePeopleModel body)
        {
            HttpClient client = new HttpClient();
            var people = JsonSerializer.Serialize(body);
            var requestContent = new StringContent(people, Encoding.UTF8, "application/json");
            string url = string.Format("http://localhost:5236/api/user/{0}", id);
            HttpResponseMessage response = client.PutAsync(url, requestContent).Result;
            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                dynamic json = JsonObject.Parse(responseString);
                return new JsonResult(json);
            }
            throw new Exception("Internal Server Error");
        }

        // DELETE api/people/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePeople(int id)
        {
            HttpClient client = new HttpClient();
            string url = string.Format("http://localhost:5236/api/user/{0}", id);
            HttpResponseMessage response = client.DeleteAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                dynamic json = JsonObject.Parse(responseString);
                return new JsonResult(json);
            }
            throw new Exception("Internal Server Error");
        }
    }
}
